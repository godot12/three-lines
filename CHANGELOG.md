## VERSIONS

### 0.1.2
- Add : Finish line, sound and replay
- Add : Walls around track
- Add : Replay on death
- Fix : Not 3 obstacles per column

### 0.1.1
- Add : Player explodes on death
- Add : Random track generation
- Add : When player dead, it stops moving

### 0.1.0
- Add : Killable player that scrolls right
- Add : Obstacle that kill player
- Add : Disgusting explosion
- Add : Rad Tilemap (it's plain white)
