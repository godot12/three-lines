extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var length := 50 
var width_track := 3
var cell_size := 100.0
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var track := self.get_node("TileMap")
	var obstacle_scene := preload("res://src/World/Obstacle.tscn")
	var tileset = track.tile_set
	var tile_id = (tileset.get_tiles_ids())[0]
	for i in range(length - 1):
		var obs := 0
		for j in range(2, width_track + 2):
			track.set_cell(i, j, tile_id)
			if randf() > 0.85 and obs < 2 and i > 3:
				obs += 1
				var obstacle := obstacle_scene.instance()
				self.add_child(obstacle)
				obstacle.position = track.to_global(track.map_to_world(Vector2(i,j))) + Vector2(-cell_size/2,cell_size/2)
	var track_end = track.to_global(track.map_to_world(Vector2(length,width_track))) + Vector2(-cell_size/2,cell_size/2)			
	var finish_area := preload("res://src/World/FinishLine.tscn")
	var finish = finish_area.instance()
	self.add_child(finish)
	var wall_scene := preload("res://src/World/Wall.tscn")
	var wall_up = wall_scene.instance()
	var wall_down = wall_scene.instance()
	wall_up.position = track.to_global(track.map_to_world(Vector2(length/2,2))) + Vector2(cell_size/2,0)
	wall_down.position = track.to_global(track.map_to_world(Vector2(length/2,5))) + Vector2(-cell_size/2,0)
	wall_up.scale.x = 500
	wall_down.scale.x = 500	
	wall_up.scale.y = 0.1
	wall_down.scale.y = 0.1
	self.add_child(wall_up)
	self.add_child(wall_down)
	finish.position = track_end - Vector2(1 * cell_size,0)
	finish.scale.y = 20
