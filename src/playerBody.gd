extends KinematicBody2D

export var speed = 200
export var scroll_acceleration = 300
var velocity = Vector2()
var alive := true
func get_input():
	velocity = Vector2()
#	if Input.is_action_pressed("right"):
#		velocity.x += 1
#	if Input.is_action_pressed("left"):
#		velocity.x -= 1
	if Input.is_action_pressed("down"):
		velocity.y += 1
	if Input.is_action_pressed("up"):
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func kill():
	alive = false
	self.remove_child(get_node("player"))
	var explosion_scene := preload("res://src/Particles2D.tscn")
	var explosion = explosion_scene.instance()
	velocity = Vector2.ZERO
	self.add_child(explosion)
	var t = Timer.new()
	t.set_wait_time(2.3)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	self.get_node("AudioStreamPlayer2D").stop()
	t.start()
	yield(t, "timeout")
	t.queue_free()
	var menu = load("res://src/Menu.tscn").instance()
	get_tree().get_root().add_child(menu)
	get_node("/root/Sample").free()
	
func win():
	print("win")
	self.kill()
	self.get_node("AudioStreamPlayer2D").play(2)
	var t = Timer.new()
	t.set_wait_time(2.3)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	self.get_node("AudioStreamPlayer2D").stop()
	t.start()
	yield(t, "timeout")
	t.queue_free()
	var menu = load("res://src/Menu.tscn").instance()
	get_tree().get_root().add_child(menu)
	get_node("/root/Sample").free()

		
	
func _physics_process(delta):
	get_input()
	if alive:
		velocity.x += scroll_acceleration
	velocity = move_and_slide(velocity)
	var number_collisions := get_slide_count()
	for index in range(number_collisions):
		var collision := get_slide_collision(index)
		if collision.collider.get_collision_layer_bit(1):
			self.kill()
		
